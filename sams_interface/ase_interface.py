import os
import copy
import datetime
import jinja2 as jinja
import pandas as pd
import tabulate
import pickle
from ase.calculators.vasp import Vasp2

# Class to easily create VASP input files from vectors of ase.Atoms objects.


class AseVaspTemplater(object):
    """
    Class to handle making VASP input files in an ASE environment.

    Use this class to handle the preparation of VASP files and Slurm submission
    scripts. Use multiple instances of the class to prepare slightly different
    versions of calculations.
    """

    def __init__(
        self,
        dir_name,
        calc_settings,
        slabs,
        use_date=True,
        other_template_path=os.path.join(
            os.path.expanduser("~"), ".vasptemplater", "templates"
        ),
        naming="formula",
    ):
        """
        Set up the class with reasonable defaults.

        Arguments
        =========

        - `dir_name`: Name of the parent directory into which the current set of
          simulation file should be written
        - `use_date`: If true, then append the current date (in YYYYMMDD format)
          to the parent directory name.
        - `other_template_path` Path to any other Jinja2 templates that should
          be rendered during input file writing.
        - `calc_settings`: A dictionary containing settings for the
          ase.calculators.vasp.Vasp2 calculator. This should be the dictionary
          form of the arguments passed to ase.calculators.vasp.Vasp2. Do not
          specify the `label` or `directory` keywords; they will be
          automatically overridded during the input file writing process.
        - `slabs`: A vector of ase.Atoms objects to be written as VASP input
          files.
        - `naming`: how to name the output folders containing each simulation.
          If "formula", each folder will be named by the
          ase.Atoms.get_chemical_formula() string of the corresponding slab. To
          manually override this, pass a list of strings with the same length as
          `slab`.

        Methods
        =======

        write_input_files()  : Write out valid VASP input files using the
        calculation settings specified in `calc_settings` and in the directory
        specified by `dir_name`.

        """

        # Current date, formatted for directory naming
        self.date_str = datetime.datetime.now().strftime("%d-%b-%Y") if use_date else ""

        # Directory to output files
        self.dir_name = "{}_{}".format(
            dir_name, self.date_str.lower()
        )  # Want to deal with lower-case month names.

        # Store calculator settings
        self.calc_settings = calc_settings

        # Store set of slabs
        self.slabs = slabs

        # Store other template path
        self.other_template_path = other_template_path

        # Store naming preference
        self.naming = naming

        if type(self.naming) is list and len(self.naming) != len(slabs):
            raise IndexError("Number of naming labels does not match number of slabs.")

    def _associate_calculator(self, slabs, calc_settings):
        """
        Associate an instance of the same Vasp2 calculator to each slab in the
        `slabs` vector, using calculator settings passed in when the class was
        instantiated.
        """

        for i, slab in enumerate(slabs):

            # Set the output directory and label
            if type(self.naming) is str and self.naming == "formula":
                job_name = slab.get_chemical_formula()
            elif type(self.naming) is list:
                job_name = str(self.naming[i])
            else:
                raise Exception(
                    """Please specify a list of strings or "formula" for the naming parameter."""
                )

            calc_settings["directory"] = os.path.join(self.dir_name, job_name)
            calc_settings["label"] = job_name
            calc_settings["atoms"] = slab

            calc = Vasp2(**self.calc_settings)

            # Set slab to have individual, named calculators that will put results in a individually named folders.
            slab.set_calculator(copy.copy(calc))

    # =====Define writer functions=====

    def _get_template(self, fname):
        with open(fname, "r") as f:
            return fname, self.env.from_string(f.read())

    def _render_to_file(self, fname, filestr):
        # Write string to file, being sure to use LF line endings (for Linux clusters)
        with open(fname, "w+", newline="\n") as dest_file:
            dest_file.write(filestr)

    def write_input_files(self):
        """
        Actually write the input files for the slabs.
        """

        # Associate calculators with slabs
        self._associate_calculator(self.slabs, self.calc_settings)

        # =====Set up Jinja environment and read templates at file write time=====
        self.env = jinja.Environment(
            autoescape=jinja.select_autoescape(["html", "xml"])
        )

        # Load templates from templates folder
        templates = {}
        template_sources = [
            the_file
            for the_file in os.listdir(self.other_template_path)
            if not os.path.isdir(os.path.join(self.other_template_path, the_file))
        ]
        for the_file in template_sources:
            templates[the_file] = self._get_template(
                os.path.join(self.other_template_path, the_file)
            )[
                1
            ]  # Returns tuple of filename and template object

        # Perform the actual write:

        for slab in self.slabs:

            calc = slab.get_calculator()

            # Write VASP input files for each system.
            calc.write_input(slab)

            # Finally, render any additional templates (e.g., the Slurm script)

            job_data = {
                "general": {"name": calc.label.split("\\")[-1]}
            }  # Make sure to pick up job name from calculator, not job_name variable.

            for fname, template in templates.items():
                # Render each template
                self._render_to_file(
                    os.path.join(calc.directory, fname), template.render(job_data)
                )


class VaspResultSet(object):
    """
    A class to represent a set of VASP results.

    Attributes
    ==========
    - `calculators`: a dictionary of the raw calculator objects associated with
      the results in `results_dir`. The dictionary is of the form {name:
      calculator}, where name is the label specified by
      `ase.calculators.vasp.Vasp2.directory`.

    Methods
    =======
    - `get_potential_energies`: retrieve the electronic potential energy from
      each simulation using ase.calculators.vasp.Vasp2.get_potential_energy().

    - `get_atoms_objects`: retrieve ase.Atoms objects for each of the
      simulations in `results_dir`.

    """

    def __init__(self, results_dir):
        """
        Set up the VaspResultSet object.

        Parameters
        ==========
        - `results_dir`: the folder from which to load VASP results. This folder
          must contain one or more directories, each of which have a minimal set
          of VASP output files (e.g., CONTCAR, OUTCAR, and vas_iwafbiawufb.xml).

        Returns
        =======
        None
        """

        self.results_dir = results_dir

        self.calculators = {
            name: Vasp2(directory=os.path.join(self.results_dir, name), restart=True)
            for name in os.listdir(self.results_dir)
            if os.path.isdir(os.path.join(self.results_dir, name))
        }

    def get_potential_energies(self):
        """
        Retrieve the electronic potential energies for each result in the set. This is
        equivalent to running `calc.get_potential_energy()` on a
        `ase.calculators.vasp.Vasp2` object.

        Parameters
        ==========
        None

        Returns
        =======
        - `potential_energies`: a dictionary of the form {name: energy}, where energy
        is the potential energy corresponding to the simulation with name (label) name.
        """

        potential_energies = {
            name: calc.get_potential_energy() for name, calc in self.calculators.items()
        }

        return potential_energies

    def get_atoms_objects(self):
        """
        Retrieve the atoms objects for all simulations in the results set. This
        is equivalent to retrieving the `ase.calculators.vasp.Vasp2.atoms` for
        each simulation in the set.

        Parameters
        ==========
        None

        Returns
        =======
        - `atoms_objects`: a dictionary of the form {name: atoms}, where atoms
          is the ase.Atoms object corresponding to the simulation with name
          (label) name.
        """

        atoms_objects = {
            name: calc.get_atoms() for name, calc in self.calculators.items()
        }

        return atoms_objects


class VaspResultsGroup(object):
    """
    A class to represent a group of VASP result sets. This works only with
    two-dimensional datasets (e.g., a grid of n * m calculations performed over
    n slab systems and m adsorption sites).

    Attributes
    ==========
    - `result_sets`: a list of the `VaspResultSet` objects, each of which
      represents a different folder of results.

    Methods
    =======
    - `get_potentialeneergy_df`: return a table of DFT potential energies,
      grouped by primary folders in the top directory and subdirectories in each
      primary directory, in pd.DataFrame format.
    - `get_potentialeneergy_md`: return a table of DFT potential energies,
      grouped by primary folders in the top directory and subdirectories in each
      primary directory, in extended Markdown format.
    - `save`: save the results group in Python pickle format.
    """

    def __init__(self, directory=None, pickle_file=None):
        """
        Create a group of VASP calculation results over a 2-dimensional grid of
        parameters represented by the folder structure. Like this:

        top_dir
        ├── site1
        │   ├── Ag36Cl
        │   ├── Au36Cl
        │   ├── ClCu36
        │   ├── ClPd36
        │   ├── ClPt36
        │   └── ClRh36
        └── site2
            ├── Ag36Cl
            ├── Au36Cl
            ├── ClCu36
            ├── ClPd36
            ├── ClPt36
            └── ClRh36

        Then, a VaspResultsGroup can represent the results like this:

        |        |    site1 |    site2 |
        | :----- | -------: | -------: |
        | Ag36Cl |  -98.661 |  -98.811 |
        | Au36Cl | -113.955 | -114.371 |
        | ClCu36 | -128.395 | -128.869 |
        | ClPd36 | -179.008 | -179.492 |
        | ClPt36 | -208.525 | -209.004 |
        | ClRh36 | -250.193 | -250.730 |

        Parameters
        ==========
        - `directory`: the parent directory containing the level 1 folders, each
          of which contain the same set of level 2 folders.
        - `pickle_file`: the Python pickle file from which to load previous
          calculation results. If both `directory` and `pickle_file` are
          specified, the pickle file takes precedence and the directory will not
          be scanned for VASP results.

        """

        # Check to see if directory and/or pickle file were specified
        if pickle_file is not None:

            # Load results from pickle file.
            with open(pickle_file, "rb") as f:
                self.results = pickle.load(f)

        elif directory is not None:

            # Load results from folder
            self.results = {
                name: VaspResultSet(results_dir=os.path.join(directory, name))
                for name in os.listdir(directory)
                if os.path.isdir(os.path.join(directory, name))
                and not name.startswith(".ipynb")  # Avoid checkpoint dirs
            }
        else:
            raise RuntimeError(
                "Please specify either a directory name or a pickle file."
            )

        self.pd_data = {
            name: result.get_potential_energies()
            for name, result in self.results.items()
        }

    def get_potentialeneergy_df(self, **kwargs):
        """
        Return a pd.DataFrame object representing the table of DFT potential
        energies.

        Parameters
        ==========
        - `kwargs`: additional arguments to pass on to the pd.DataFrame
          constructor.

        Returns
        =======
        - `pd_results`: a pd.DataFrame object representing the potential
          energies of the contained simulations.

        """

        pd_results = pd.DataFrame(data=self.pd_data, **kwargs)

        return pd_results

    def get_potentialeneergy_md(self, **kwargs):
        """
        Return a string representing the table of DFT potential energies, in
        Markdown format.

        Parameters
        ==========
        - `kwargs`: additional arguments to pass on to the tabulate.tabulate
          function.

        Returns
        =======
        - `md_string`: a string representing the potential energies of the
          contained simulations as a Markdown table.

        """

        pd_results = self.get_potentialeneergy_df()

        md_string = tabulate.tabulate(
            pd_results, tablefmt="pipe", floatfmt=".3f", headers="keys", **kwargs
        )

        return md_string

    def save(self, file, **kwargs):
        """
        Save the results group to the file `file` on disk, in Python pickle
        format.

        Parameters
        ==========
        - `file`: the path to which to save the pickle file.
        - `kwargs`: additional arguments to pass to the pickle.dump() function.
        """

        with open(file, "wb") as f:
            pickle.dump(self.results, f)
