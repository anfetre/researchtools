from ase.io import read
from ase.visualize import view

image_filename = '../resources/vaspruns/vasprun_rh5-tio2__.xml'
images = read(image_filename, index=':')

view(images)
