from amp import Amp
from amp.model.neuralnetwork import NeuralNetwork
from amp.descriptor.gaussian import Gaussian
from ase.io import read
import time


label = 'mac_amp2'
LOAD = False

if LOAD:
    calc = Amp.load(label + '.amp')
else:

    calc = Amp(descriptor=Gaussian(), model=NeuralNetwork(), label=label, )
    images_filename = '../resources/vaspruns/vasprun_rh5-tio2.xml'

    images = read(images_filename, index=':')

    print('training on %s (%d images)' % (images_filename, len(images)))

    start_time = time.time()

    calc.train(images=images)

    finish_time = time.time()

    print('%s trained after %f seconds' % (calc.label, finish_time - start_time))

print('Amp calculator ready:', calc)
