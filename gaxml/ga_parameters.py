class SystemParameters:

    def __init__(self, composition, min_bond_dist, max_bond_dist, preferred_bond_dist,
                 required_bonds=2, support=None, gen_breadth=5):
        self.composition = composition
        self.min_bond_dist = min_bond_dist
        self.max_bond_dist = max_bond_dist
        self.preferred_bond_dist = preferred_bond_dist
        self.required_bonds = required_bonds
        # self.metal_nonmetal_bond_dist = metal_nonmetal_bond_dist
        # self.metal_metal_bond_dist = metal_metal_bond_dist
        self.support = support
        self.gen_breadth = gen_breadth


class GenerationParameters:

    def __init__(self, small_cluster_cutoff=5, gen_breadth=5, small_cluster_max_gens=50000,
                 large_cluster_max_gens=10000, facet_attempts=5, max_fluctuation=.1,
                 cross_max_gens=5000, mutation_max_gens=5000):
        self.small_cluster_cutoff = small_cluster_cutoff
        self.gen_breadth = gen_breadth
        self.small_cluster_max_gens = small_cluster_max_gens
        self.large_cluster_max_gens = large_cluster_max_gens
        self.facet_attempts = facet_attempts
        self.max_fluctuation = max_fluctuation
        self.cross_max_gens = cross_max_gens
        self.mutation_max_gens = mutation_max_gens


class AlgorithmParameters:

    def __init__(self, conv_crit={'iterations': 20, 'stagnation': 5}, keep_best=.3, cross_prob=.7, mutation_prob=.3):
        self.conv_crit = conv_crit
        self.keep_best = keep_best
        self.cross_prob = cross_prob
        self.mutation_prob = mutation_prob
