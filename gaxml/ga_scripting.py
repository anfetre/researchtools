def slurm_job(name, nodes_requested, processors_per_node, walltime,
              email_address, commands, out_filename=None, err_filename=None,
              mail_types='ALL', quality_of_service='regular',
              licence='SCRATCH', constraint='haswell',
              shebang='#!/bin/bash -l', filename=None):
    slurm_header = '#SBATCH '
    linux_end_line = '\n'

    out_filename = name + '.out' if out_filename is None else out_filename
    err_filename = name + '.err' if err_filename is None else err_filename

    def new_line(text, option='', header=slurm_header, comment='', end_line=linux_end_line):
        return header + option + str(text) + comment + end_line

    script = [new_line(shebang, header=''),
              new_line(name, '-J '),
              new_line(nodes_requested, '-N '),
              new_line(processors_per_node, '--ntasks-per-node='),
              new_line(quality_of_service, '-q '),
              new_line(walltime, '-t '),
              new_line(email_address, '--mail-user='),
              new_line(mail_types, '--mail-type='),
              new_line(out_filename, '-o '),
              new_line(err_filename, '-e '),
              new_line(licence, '-L '),
              new_line(constraint, '-C '),
              linux_end_line
              ]

    for command in commands:
        script.append(new_line(command, header=''))

    if filename is not None:
        script = ''.join(script)
        with open(filename, 'w', newline='\n') as f:
            f.write(script)
        return script
    else:
        return ''.join(script)


default_commands = ['export VASP_PP_PATH=$HOME/potential',
                    'export ASE_VASP_VDW=$HOME/vdw_kernel',
                    'module load python3',
                    'source activate conda_env_test',
                    'module load vasp',
                    'python3 ase_vasp_demount.py >> ase_vasp_demount.out']

default_script = slurm_job('ase_vasp_demount', 2, 64,
                           '00:30:00', 'ericmusa@umich.edu',
                           default_commands, quality_of_service='debug',
                           filename='ase_vasp_demount.sh')
