from gaxml.ga_supports import *
from gaxml.ga_parameters import *


anatase_support = build_support('resources/cifs/TiO2_mp-390_conventional_standard.cif', [1, 0, 1], [2, 4, 2], 10)

anatase_support = trim_atoms(anatase_support, 12, 16)
anatase_support = fix_atoms(anatase_support)

system_params_rh_tio2 = SystemParameters(composition=('Rh', 10), min_bond_dist=1.7, max_bond_dist=3.1,
                                         preferred_bond_dist=2.2, support=anatase_support)


def rh_on_tio2(n_cluster=5):
    return SystemParameters(composition=('Rh', n_cluster), min_bond_dist=1.7, max_bond_dist=3.1,
                            preferred_bond_dist=2.2, support=anatase_support)


alumina_support = build_support('resources/cifs/Al2O3_mp-1143_conventional_standard.cif', [1, 0, 0], [3, 1, 1], 10)

alumina_support = trim_atoms(alumina_support, 11, 14)
alumina_support = fix_atoms(alumina_support)
del alumina_support[3]


def pd_on_al2o3(n_cluster=5):
    return SystemParameters(composition=('Pd', n_cluster), min_bond_dist=1.7, max_bond_dist=2.9,
                            preferred_bond_dist=2.5, support=alumina_support)
