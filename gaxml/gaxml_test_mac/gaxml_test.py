from gaxml.gaxml import *
from gaxml.ga_supports import *
from systems import *

rh_tio2_settings = {"command": "srun -E vasp_std",
                    'lcharg': False,
                    'lwave': False,
                    'prec': 'Normal',
                    'encut': 300,
                    'lmaxmix': 4,
                    'icharg': 1,
                    'idipol': 3,
                    'ispin': 2,
                    'lorbit': 11,
                    'ivdw': 11,
                    'potim': 0.15,
                    'ibrion': 3,
                    'isif': 0,
                    'ediffg': -0.05,
                    'algo': 'fast',
                    'lreal': 'auto',
                    'nsim': 1,
                    'lplane': False,
                    'npar': 16,
                    'nelmin': 4,
                    'nsw': 800,
                    'ismear': 0,
                    'sigma': 0.2,
                    'ldau': 't',
                    'ldauu': (2.5, 0, 0),
                    'ldauj': (0, 0, 0),
                    'ldaul': (2, -1, -1),
                    'amix': 0.20,
                    'bmix': 0.0010,
                    'amix_mag': 0.80,
                    'bmix_mag': .0010
                    }

n_cluster = 5
ga = GeneticAlgorithm('gaxml_emt', 3, pd_on_al2o3(n_cluster))

ga.run()
