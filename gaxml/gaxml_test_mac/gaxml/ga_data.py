__author__ = 'ERIC R MUSA'
__date__ = '06 JUNE 2019'


import sqlite3
from typing import Dict


class PrimKeyInt(type):
    pass


def type_to_str(dtype) -> str:
    if dtype == str:
        return 'text'
    elif dtype == int:
        return 'int'
    elif dtype == float:
        return 'real'
    elif dtype == PrimKeyInt:
        return 'int primary key'
    else:
        return 'blob'


def str_to_type(string) -> type:
    # if string == 'text':
    #     return str
    # el
    if string == 'int primary key':
        return PrimKeyInt
    if string == 'int':
        return int
    elif string == 'real':
        return float
    else:
        return str


def get_organization(columns, with_types=False) -> str:
    organization = "("

    if with_types:
        for i, column in enumerate(columns.items()):
            if i + 1 == len(columns):
                organization += '%s %s)' % (column[0], type_to_str(column[1]))
            else:
                organization += '%s %s, ' % (column[0], type_to_str(column[1]))
    else:
        for i, column in enumerate(columns.items()):
            if i + 1 == len(columns):
                organization += ':%s)' % column[0]
            else:
                organization += ':%s, ' % column[0]
    return organization


class TableManager:

    def __init__(self, db_manager, table_name: str, columns=None):
        self.db_manager = db_manager

        self.table_name = table_name
        self.db_manager.cursor.execute('pragma table_info(%s)' % table_name)
        if columns is None:
            self.columns = {col[1]: str_to_type(col[2]) for col in self.db_manager.cursor.fetchall()}
        else:
            self.columns = columns

    def add_index(self, index_name: str, index_column: str):
        command = "create index if not exists %s on %s(%s)" % (index_name, self.table_name, index_column)
        with self.db_manager.conn:
            self.db_manager.cursor.execute(command)

    def add_column(self, column_name: str, column_type: type):
        with self.db_manager.conn:
            self.db_manager.cursor.execute('alter table %s add column %s %s' % (self.table_name, column_name,
                                                                     type_to_str(column_type)))
        self.columns[column_name] = column_type

    def add_row(self, item: dict):
        item_type = {k: type(v) for k, v in item.items()}
        # if item_type != self.columns:
        #     return
        for col_name, col_type in self.columns.items():
            if col_name not in item:
                item[col_name] = None
            else:
                if col_type != item_type[col_name] and item[col_name] is not None:
                    raise ValueError('%s should be %a but is %a' % (item[col_name], col_type, item_type[col_name]))
        if len(item) > len(self.columns):
            print(item_type)
            print(self.columns)
            raise ValueError("item has more values than are allowed by this table's columns")
        time_stamp = item['time_stamp']
        with self.db_manager.conn:
            # print(item_type)
            # print(self.columns)
            command = 'insert or ignore into %s values %s' % (self.table_name, get_organization(self.columns, False))
            # print(command)
            self.db_manager.cursor.execute(command, item)
        # for k, v in item.items():
        #     self.notify_listeners(k, time_stamp)

    def update_row(self, update: dict):
        time_stamp = update.pop('time_stamp')
        update_string = ""
        for i, prop in enumerate(update.items()):
            if prop[0] not in self.columns:
                self.add_column(prop[0], type(prop[1]))
            if i + 1 < len(update):
                update_string += "%s = :%s, " % (prop[0], prop[0])
            else:
                update_string += "%s = :%s" % (prop[0], prop[0])
        with self.db_manager.conn:
            command = 'update %s set %s where time_stamp = %d' % \
                      (self.table_name, update_string, time_stamp)
            self.db_manager.cursor.execute(command, update)

    def row_exists(self, primary_key: str, item_key: int):
        self.db_manager.cursor.execute('select exists(select * from %s where %s = %d)' %
                                       (self.table_name, primary_key, item_key))
        return bool(self.db_manager.cursor.fetchall())

    def get_row(self, primary_key: str, item_key: int):
        self.db_manager.cursor.execute('select * from %s where %s = %d' % (self.table_name, primary_key, item_key))
        return self.row_to_dict(self.db_manager.cursor.fetchone())

    # def get_column(self, column: str):
    #     self.db_manager.cursor.execute('select * from %s where %s = %d' % (self.table_name, primary_key, item_key))

    def row_to_dict(self, row: tuple) -> dict:
        d = {}
        if len(row) != len(self.columns):
            raise ValueError('row does not match recorded columns')
        for i, item in enumerate(row):
            d[list(self.columns.keys())[i]] = item
        return d


class DBManager:

    def __init__(self, db_name: str, default_columns: Dict[str, type]):
        self.conn = sqlite3.connect(db_name)
        self.cursor = self.conn.cursor()
        self.default_columns = default_columns
        tables = self.cursor.execute('select name from sqlite_master where type="table"').fetchall()
        self.tables = [item[0] for item in tables]
        self.managers: Dict[str, TableManager] = {}

    def add_table_manager(self, table_manager: TableManager):
        self.managers[table_manager.table_name] = table_manager
        self.tables.append(table_manager.table_name)
        return table_manager

    def reconspect_table_manager(self, table_name, columns=None):
        if columns is None:
            columns = self.default_columns
        command = "create table if not exists %s %s" % (table_name, get_organization(columns, True))
        # print(command)
        with self.conn:
            self.cursor.execute(command)
        return self.add_table_manager(TableManager(self, table_name))
