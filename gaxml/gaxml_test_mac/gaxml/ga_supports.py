from ase import Atoms
from ase.constraints import FixAtoms
from ase.build import surface, add_vacuum
from ase.io import read


def build_support(cif_filename: str, indices, size, vacuum=0):
    """
    Construct an ASE Atoms object representing a crystalline support structure from a .cif file from MP

    :param cif_filename: .cif filename containing crystal structure data from Materials Project
    :param indices: desired (h, k, l) Miller indices of support
    :param size: repetitions of the unit cell in the (x, y, z) axes
    :param vacuum: size of vacuum above and below the support in the z axis
    :return: Atoms of the support (including the cell and info)
    """
    lattice = read(cif_filename)
    pre_support = surface(lattice=lattice, indices=indices, layers=size[2], vacuum=vacuum)
    # view(support)
    pre_support *= (size[0], size[1], 1)

    components = {}
    for atom in pre_support:
        if atom.symbol in components:
            components[atom.symbol].append(atom)
        else:
            components[atom.symbol] = [atom]

    support = Atoms()
    for component in components.values():
        support += Atoms(component)
    support.cell = pre_support.cell
    support.info = pre_support.info
    # view(support)
    # add_vacuum(support, vacuum)
    return support


def fix_atoms(atoms):
    """
    Put the FixAtoms constraint on the passed atoms object.

    :param atoms: Atoms object whose components will be fixed
    :return: copy of atoms with all components fixed
    """
    atoms = atoms.copy()
    atoms.set_constraint(FixAtoms(mask=[True for _ in range(len(atoms))]))
    return atoms


def trim_atoms(atoms, lower, upper, axis=2):
    """
    Remove the atoms outside of the upper and lower bounds from the Atoms object passed.

    :param atoms: Atoms object
    :param lower: lower coordinate limit of atoms to keep
    :param upper: upper coordinate limit of atoms to keep
    :param axis: the direction (0=x, 1=y, 2=z) in which to trim
    :return:
    """
    atoms = atoms.copy()
    to_del = []
    for i, atom in enumerate(atoms):
        if not (lower < atom.position[axis] < upper):
            to_del.append(i)

    to_del.sort(reverse=True)
    while to_del:
        del atoms[to_del.pop(0)]

    return atoms
