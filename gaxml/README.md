GAxML
=====
A genetic optimization algorithm accelerated with machine learning.
Designed for optimizing both gas-phase and supported nanostructures.

-----
Goldsmith Lab
=============
University of Michigan, Ann Arbor
Department of Chemical Engineering
-------------
start date: 15 Jan 2019

code author: Eric Musa

-------------



