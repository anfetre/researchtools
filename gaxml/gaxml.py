# -*- coding: utf-8 -*-
# import numpy as np
from ase.calculators.emt import EMT
from ase.calculators.vasp.vasp2 import Vasp2
from ase.io import write
from gaxml.ga_schemata import *
import os
import time


class GeneticAlgorithm:

    # self.pop = Population(params, support)
    # self.schema? = schema
    # self.generation_schema = generation_schema
    # self.evaluation_schema = evaluation_schema
    # self.nested_loop = nested_loop

    def __init__(self, ga_name: str, pop_size, system_params: SystemParameters,
                 algo_params: AlgorithmParameters = None, gen_params: GenerationParameters = None,
                 vasp_settings=None, relaxations_dir='relaxations',  # support=None,
                 production_schema: ProductionSchema = None, logger: Logger = None,
                 tracker: Tracker = None, minima_dir='minima'):
        self.ga_name = ga_name
        self.pop_size = pop_size
        self.sys_params = system_params
        self.algo_params = AlgorithmParameters() if algo_params is None else algo_params
        self.gen_params = GenerationParameters() if gen_params is None else gen_params
        self.vasp_settings = vasp_settings
        self.relaxations_dir = relaxations_dir
        # self.support = support
        self.logger = Logger(ga_name + "_log.txt") if logger is None else logger
        self.tracker = Tracker(ga_name, ga_name + '_stats') if tracker is None else tracker
        self.gene_pool = GenePool(self.sys_params, self.gen_params, self.logger, self.tracker, self.sys_params.support)
        self.production_schema = ProductionSchema(self.gene_pool, self.select, self.algo_params, self.logger,
                                                  self.tracker) if production_schema is None else production_schema
        self.age = 0

        self.relaxed_individuals: List[Individual] = []
        self.unrelaxed_individuals: List[Individual] = []

        self.minima_dir = minima_dir

    def fill_individuals(self):
        if len(self.relaxed_individuals) + len(self.unrelaxed_individuals) == self.pop_size:
            return
        elif len(self.relaxed_individuals) + len(self.unrelaxed_individuals) > self.pop_size:
            raise GAException('there are too many individuals being operated on (r: %d, u: %d)' %
                              (len(self.relaxed_individuals), len(self.unrelaxed_individuals)))

        self.logger.log('filling individuals - beginning number of individuals: relaxed - %d, unrelaxed - %d' %
                        (len(self.relaxed_individuals), len(self.unrelaxed_individuals)))

        while len(self.relaxed_individuals) + len(self.unrelaxed_individuals) < self.pop_size:
            attempt_individual_insertion(self.gene_pool.generate_cluster(), self.unrelaxed_individuals,
                                         'unrelaxed individuals', self.logger)

        self.logger.log('final number of individuals: relaxed - %d, unrelaxed - %d' %
                        (len(self.relaxed_individuals), len(self.unrelaxed_individuals)))

    def associate_vasp_calculator(self, cluster: Individual):
        settings = self.vasp_settings
        settings["directory"] = os.path.join(self.relaxations_dir, cluster.name)
        settings["label"] = cluster.name
        settings["atoms"] = structure = cluster.get_supported_structure()

        calc = Vasp2(**settings)
        structure.set_calculator(calc)
        return structure, calc

    def dft_relax_structure(self, cluster: Individual):
        """
        submit a cluster's structure for DFT relaxation
        :param cluster: cluster whose structure will be relaxed
        :return: returns whether or not the structure was successfully relaxed
        """

        class VaspRelaxation(Tracker.Trackable):

            def __init__(self, subject, start, finish):
                super().__init__(self.__class__.__name__, subject, start, finish)

        start_time = time.time()

        assert isinstance(cluster, Individual)

        # TODO distinguish between incomplete and failed DFT calls
        #   incomplete - try again n times
        #   failed - mutate and try again n times (or other shit)

        structure, calc = self.associate_vasp_calculator(cluster)

        structure.get_potential_energy()

        finish_time = time.time()

        cluster = self.gene_pool.demount(cluster, structure, structure.get_potential_energy())

        self.tracker.track(VaspRelaxation(cluster.name, start_time, finish_time))

        return cluster

    def emt_check_structure(self, cluster: Individual):
        """
        submit a cluster's structure for EMT energy calculation (for debugging)
        :param cluster: cluster whose potential energy will be calculated
        :return: returns whether or not the structure' energy was successfully relaxed
        """

        class EMTEvaluation(Tracker.Trackable):

            def __init__(self, subject, start, finish):
                super().__init__(self.__class__.__name__, subject, start, finish)

        start_time = time.time()

        assert isinstance(cluster, Individual)

        structure = cluster.get_supported_structure()

        assert isinstance(structure, Atoms)

        structure.set_calculator(EMT())
        energy = structure.get_potential_energy()
        cluster.energy = energy
        cluster.is_relaxed = True

        self.logger.log('structure energy: %f' % energy)

        finish_time = time.time()

        self.tracker.track(EMTEvaluation(cluster.name, start_time, finish_time))

        return cluster

    @staticmethod
    def tanh_fitness_function(energy, energy_max, energy_min):
        if energy_max == energy_min:
            return 1.0
        return .5 - .5 * np.tanh(2 * (energy - energy_min) / (energy_max - energy_min) - 1)

    def evaluate(self, distance_disparity_threshold=1.0, energy_equivalence_threshold=.05):
        """
        First ensures all individuals have undergone DFT relaxation and have been sorted by energy.
        Then inscribes each individual with a "fitness" with a TANH function using the highest and
        lowest energies in the group, and the individual's energy.

        Will take a long time due to all the DFT calls (for the time being)

        """
        # TODO streamline handling self.individuals

        self.logger.log('beginning evaluation of gen %d' % self.age)
        self.fill_individuals()

        duplicates = 0
        while self.unrelaxed_individuals:
            self.logger.log('there are {} unrelaxed individuals remaining'.format(len(self.unrelaxed_individuals)))
            individual = self.unrelaxed_individuals.pop(0)
            assert isinstance(individual, Individual)
            if individual.is_relaxed:
                self.logger.log('individual is actually relaxed!')
                if not attempt_individual_insertion(individual, self.relaxed_individuals, 'relaxed individuals',
                                                    self.logger, distance_disparity_threshold,
                                                    energy_equivalence_threshold):
                    duplicates += 1
                    self.logger.log('replacing duplicate with unrelaxed individual')
                    self.unrelaxed_individuals.append(self.gene_pool.generate_cluster())
            else:
                self.logger.log('relaxing unrelaxed structure')
                relaxed_individual = self.emt_check_structure(individual) if self.vasp_settings is None \
                    else self.dft_relax_structure(individual)
                if not attempt_individual_insertion(relaxed_individual, self.relaxed_individuals,
                                                    'relaxed_individuals', self.logger,
                                                    distance_disparity_threshold, energy_equivalence_threshold):
                    duplicates += 1
                    self.logger.log('replacing duplicate with unrelaxed individual')
                    self.unrelaxed_individuals.append(self.gene_pool.generate_cluster())
            if duplicates > 10:
                raise GAException('10 duplicate structures rejected, GA is stagnated or stuck')

        self.logger.log('population of %d created - duplicates: %d' %
                        (len(self.relaxed_individuals), duplicates))

        self.relaxed_individuals.sort(key=lambda c: c.energy)
        self.logger.log("individuals' energies:")
        self.logger.log(' '.join(['%5.5f' % ind.energy for ind in self.relaxed_individuals]))

        assert len(self.unrelaxed_individuals) == 0
        assert len(self.relaxed_individuals) == self.pop_size

        energies = [individual.energy for individual in self.relaxed_individuals]

        energy_max = max(energies)
        energy_min = min(energies)

        self.logger.log("individuals' fitnesses:")
        # calculate the fitness
        for individual in self.relaxed_individuals:
            individual.fitness = GeneticAlgorithm.tanh_fitness_function(individual.energy,
                                                                        energy_max, energy_min)
        self.logger.log(' '.join(['%5.5f' % ind.fitness for ind in self.relaxed_individuals]))

    def get_fitnesses(self) -> Dict[str, float]:
        """
        Returns dict of names: fitnesses of all individuals

        :return: dictionary with the fitnesses of all individuals
        """
        # TODO fix roulette to take arbitrary keys as options
        return {str(i): ind.fitness for i, ind in enumerate(self.relaxed_individuals)}

    def select(self, working_set=False) -> Individual:
        """
        function that selects an individual from the pool based on fitness using a weighted roulette

        :return: selected individual
        """
        if working_set:
            assert len(self.relaxed_individuals) > 0
            # TODO fix roulette to take arbitrary keys as options
            total_fitness = 0
            selection_probs = []
            for individual in self.relaxed_individuals:
                total_fitness += individual.fitness
                selection_probs.append(total_fitness)
            selection_probs = [cumulative_fitness / total_fitness for cumulative_fitness in selection_probs]
            prob = np.random.rand()
            index = 0
            for i, selection_prob in enumerate(selection_probs):
                if prob < selection_prob:
                    index = i
                    break
            self.logger.log('selected index: {} - selected probability: {}'.format(index, selection_probs[index]))
            return self.relaxed_individuals[index]
        else:
            assert len(self.gene_pool.minima) > 0
            total_fitness = 0
            selection_probs = []
            for individual in self.gene_pool.minima:
                total_fitness += individual.fitness
                selection_probs.append(total_fitness)
            selection_probs = [cumulative_fitness / total_fitness for cumulative_fitness in selection_probs]
            prob = np.random.rand()
            index = 0
            for i, selection_prob in enumerate(selection_probs):
                if prob < selection_prob:
                    index = i
                    break
            self.logger.log('selected index: {} - selected probability: {}'.format(index, selection_probs[index]))
            return self.gene_pool.minima[index]

    def evolve(self):
        """
        evaluates all individuals first, filling in selector probabilities

        then a proportion of top individuals to keep

        fills the remaining slots with individuals who have been crossed, mutated, or some combination of both

        :return: None
        """

        self.logger.log('evolving at age %d' % self.age)

        if self.unrelaxed_individuals:
            self.evaluate()

        if self.relaxed_individuals:
            self.logger.log('there are {} relaxed individuals'.format(len(self.relaxed_individuals)))
            for individual in self.relaxed_individuals:
                attempt_individual_insertion(individual, self.gene_pool.minima, 'gene pool minima', self.logger)

            n_keep = int(self.pop_size * self.algo_params.keep_best)
            n_keep = 1 if n_keep == 0 else n_keep
            self.logger.log('keeping best %d of population for next generation' % n_keep)

            self.relaxed_individuals = self.relaxed_individuals[:n_keep]

            # TODO clean up logic with cross/mutate combinations

            assert len(self.relaxed_individuals) == n_keep
            assert len(self.unrelaxed_individuals) == 0

            n_new_individuals = self.pop_size - len(self.relaxed_individuals)
            new_individuals = self.production_schema(n_new_individuals)

            self.unrelaxed_individuals += new_individuals

            assert len(self.relaxed_individuals) == n_keep
            assert len(self.unrelaxed_individuals) == self.pop_size - n_keep
            assert all([isinstance(offspring, Individual) for offspring in
                        self.unrelaxed_individuals + self.relaxed_individuals])

        else:
            self.logger.log('there are no relaxed individuals, filling')
            self.fill_individuals()

        self.logger.log('next generation is ready to go - finished age %d' % self.age)
        self.iterate_age()

        self.evaluate()

    def iterate_age(self):
        self.age += 1
        self.tracker.iterate()

    def check_convergence_criteria(self):
        for conv_type, criteria in self.algo_params.conv_crit.items():
            if conv_type == 'iterations' and criteria <= self.age:
                return True
            elif conv_type == 'stagnation' and criteria <= self.gene_pool.ikoth:
                return True

        return False

    def write_best_individuals(self, dirname):
        if not os.path.isdir(dirname):
            os.makedirs(dirname)
        n_keep_best = int(len(self.unrelaxed_individuals) * self.algo_params.keep_best)
        n_keep_best = 1 if n_keep_best == 0 else n_keep_best
        for individual in self.unrelaxed_individuals[:n_keep_best]:
            write(os.path.join(dirname, 'POSCAR.%s' % individual.name), individual.get_supported_structure())

    def run(self):
        """
        run the GA - evolve the population self.generation_max times

        :return: None
        """

        while not self.check_convergence_criteria():
            self.evolve()

        self.gene_pool.write_minima(self.algo_params.keep_best, self.minima_dir)
