import numpy as np
from typing import List, Tuple, Union, Any
from ase import Atom, Atoms


def atoms_from_coordinates(symbol: str, coordinates: np.ndarray):
    return Atoms([symbol for _ in range(len(coordinates))], coordinates)


def get_center(arr) -> np.ndarray:
    if isinstance(arr, Atoms):
        arr = arr.positions
    elif isinstance(arr, Atom):
        return arr.position.copy()
    else:
        if len(arr.shape) == 1:
            if arr.shape == (3,):
                return arr
            else:
                raise ValueError('arr has to be an Nx3 matrix for 3d coordinates')
    return sum(arr) / len(arr)


def distance(a, b):
    a = np.array(a)
    b = np.array(b)
    return np.sqrt(((a - b) ** 2).sum())


# def shift(structure: Atoms, u: np.ndarray):
#     structure = Atoms(structure)
#     for atom in structure:
#         atom.position += u
#     return structure


# def distance_between_two_atoms(a: Atom, b: Atom) -> float:
#     return distance(a.position, b.position)


def get_bonds(a, b=None, verbose=False, indices=False) -> List[Tuple[float, Any, Any]]:
    if isinstance(a, Atoms):
        a_coords = a.positions.copy()
    elif isinstance(a, Atom):
        a_coords = np.array([a.position])
        if b is None:
            return [(0.0, a, a)]
        a = [a]
        # print(a_coords)
    else:
        a_coords = a.copy()
        if len(a_coords.shape) == 1:
            # print('1d shape a coords', a_coords)
            a_coords = np.array([a_coords])
            a = np.array([a])
            if b is None:
                return [(0.0, a_coords, a_coords)]
            # print('fixed', a_coords)
    # print('A', A)
    # print('B', B)
    bonds = []
    if b is None:
        for i, atom_1 in enumerate(a_coords):
            for j, atom_2 in enumerate(a_coords):
                if j > i:
                    if indices:
                        bonds.append((distance(atom_1, atom_2), i, j))
                    else:
                        bonds.append((distance(atom_1, atom_2), a[i], a[j]))
    else:
        if isinstance(b, Atoms):
            b_coords = b.positions.copy()
        elif isinstance(b, Atom):
            b_coords = np.array([b.position])
            b = [b]
            # print(b_coords)
        else:
            b_coords = b.copy()
            if len(b_coords.shape) == 1:
                # print('1d shape a coords', b_coords)
                b_coords = np.array([b_coords])
                b = np.array([b])
                # print('fixed', b_coords)

        if verbose:
            print('a', a)
            print('b', b)

        for i, atom_a in enumerate(a_coords):
            for j, atom_b in enumerate(b_coords):
                if indices:
                    bonds.append((distance(atom_a, atom_b), i, j))
                else:
                    bonds.append((distance(atom_a, atom_b), a[i], b[j]))
    bonds.sort(key=lambda bond: bond[0])
    return bonds


# def get_center(structure: np.ndarray) -> np.ndarray:
#     """
#     Takes in coordinates of structure and returns coordinates of centered structure.
#
#     DOES NOT MODIFY STRUCTURE
#
#     :param structure: pre-centered structure
#     :return: post-centered structure
#     """
#     return sum(structure) / len(structure)


def test_shortest_bond(structure, max_bond_dist, min_bond_dist, verbose=False) -> bool:
    """
    checks if the shorted bond length between atoms within a structure falls outside of the specified range

    :param structure: structure being checked
    :param max_bond_dist: maximum length that the minimum structure distance can have
    :param min_bond_dist: minimum length that the minimum structure distance can have
    :param verbose: whether or not to print diagnostics
    :return: if structure's min distance is outside of the range
    """
    # bonds = structure.get_all_distance()
    bonds = get_bonds(structure)
    # print(bonds[0])
    # shortest_distance = min([min(row) for row in bonds])
    shortest_distance = bonds[0][0]
    if verbose:
        print(bonds[0])
    return shortest_distance > max_bond_dist or shortest_distance < min_bond_dist


def test_for_bad_bonds(structure, max_bond_dist, min_bond_dist, n_required_bonds, verbose=False) -> bool:
    """
    Check if all atoms in the structure have the required number of bonds

    :param structure: coordinates of atoms in a structure
    :param max_bond_dist: max distance that a cluster-nonmetal bond may be
    :param min_bond_dist: min distance that a cluster-nonmetal bond may be
    :param n_required_bonds: minimum number of bonds (distances) that have to be within the specified range
    :param verbose: whether or not to print diagnostics
    :return: returns if the structure does not satisfy bonding criteria
    """

    for atom in structure:
        bonds = get_bonds(atom, structure)  # shortest bond will be between 'atom' and 'atom'
        shortest_bond = bonds[1]  # 0th index will have distance = 0.0, 1st index will be shortest real bond
        # print('shortest bond', shortest_bond)
        nth_bond = bonds[n_required_bonds]  # n_required_bonds index will be nth real bond
        # print('nth bond', nth_bond)
        if shortest_bond[0] < min_bond_dist:
            if verbose:
                print('shortest bond is too short:', shortest_bond)
            return True
        elif shortest_bond[0] > max_bond_dist:
            if verbose:
                print('shortest bond is too long:', shortest_bond)
            return True
        elif nth_bond[0] > max_bond_dist:
            if verbose:
                print('nth bond is too long:', nth_bond)
            return True

    return False


def match_structures(structure, template) -> Tuple[List, float]:
    """
    :param structure: structure to be fit to template
    :param template: template that structure will be attempted to be fit to
    :return: list of indices of structure corresponding to the indices [0, 1, ... n] of template and the total disparity
    """

    if isinstance(structure, Atoms):
        structure = structure.positions.copy()
    elif isinstance(structure, Atom):
        structure = np.array([structure.position])
    else:
        if len(structure.shape) == 1:
            structure = np.array([structure])
        else:
            structure = structure.copy()

    if isinstance(template, Atoms):
        template = template.positions.copy()
    elif isinstance(template, Atom):
        template = np.array([template.position])
    else:
        if len(template.shape) == 1:
            template = np.array([template])
        else:
            template = template.copy()

    if len(structure) != len(template):
        raise ValueError("structure and template must have same number of atoms (s: {}, t: {})".format(len(structure),
                                                                                                       len(template)))
    elif len(structure) == 1 and len(template) == 1:
        return [0], 0.0

    structure -= get_center(structure)
    template -= get_center(template)
    unmatched = [i for i in range(len(structure))]
    matched_atoms = [None for _ in range(len(template))]
    total_disparity = 0.0

    for i, coord in enumerate(template):
        bonds = get_bonds(coord, structure, indices=True)
        for bond in bonds:
            if bond[2] in unmatched:
                matched_atoms[i] = bond[2]
                unmatched.remove(bond[2])
                total_disparity += bond[0]
                break

    return matched_atoms, total_disparity


def attempt_individual_insertion(candidate, pool, pool_name, logger,
                                 distance_disparity_threshold=1,
                                 energy_equivalence_threshold=.05):
    def compare_energy(a, b, threshold):
        """
        make sure, if a and b both have energies, that their energies are different
        :param a: a
        :param b: b
        :param threshold: energy threshold
        :return:
        """
        if a.energy is not None and b.energy is not None:
            return abs(a.energy - b.energy) > threshold
        return True
    logger.log('attempting to add candidate {} to {}'.format(candidate.name, pool_name))
    if pool is None:
        raise ValueError('pool must be list of individuals')
    if not pool:
        pool.append(candidate)
        logger.log('{} was added to {} ({})'.format(candidate.name, pool_name, len(pool)))
        return True
    else:
        duplicate = False
        for individual in pool:
            matches, disparity = match_structures(candidate.get_supported_structure(),
                                                  individual.get_supported_structure())
            if not (disparity > distance_disparity_threshold and
                    compare_energy(candidate, individual, energy_equivalence_threshold)):
                duplicate = True
                break
        if not duplicate:
            pool.append(candidate)
            logger.log('{} was added to {} ({})'.format(candidate.name, pool_name, len(pool)))
            return True
        else:
            logger.log('{} was rejected from {} ({})'.format(candidate.name, pool_name, len(pool)))
            return False


# def equivalent_structures_assessment(a, b, tolerance=.5) -> Tuple[bool, float, dict]:
#     """
#     Checks to see if two structures are equivalent by matching each atom from 'a' to an atom from 'b'.
#     Matched atoms are those such that their distances are minimized.
#     Two structures are equivalent if the total of the distances between paired atoms is less than the tolerance
#     :param a: structure a
#     :param b: structure b
#     :param tolerance: maximum total distance allowed before structures are considered not equivalent
#     :return: whether or not a and b are equivalent and the total distances between paired atoms
#     """
#     # TODO could add metrics to see how similar the structures are
#     if len(a) != len(b):
#         raise ValueError("a and b must have same number of atoms")
#     a = a.copy()
#     b = b.copy()
#     a.positions -= get_center(a)
#     b.positions -= get_center(b)
#     # a = Atoms([Atom(atom_a.symbol, atom_a.position - a_center) for atom_a in a])
#     # b = Atoms([Atom(atom_b.symbol, atom_b.position - b_center) for atom_b in b])
#     matched_atoms = {i: None for i in range(len(a))}
#     unmatched = []
#     total_disparity = 0.0
#
#     for i in range(len(a)):
#         distances_to_b = get_bonds(a[i], b, indices=True)
#         min_bond = distances_to_b[0]
#         matched_atoms[i] = min_bond[2]
#         total_disparity += min_bond[0]
#
#     to_del = []
#     for k, v in matched_atoms.items():
#         if v is None:
#             to_del.append(k)
#     while to_del:
#         matched_atoms.pop(to_del.pop(0))
#     return total_disparity < tolerance, total_disparity, matched_atoms

def fluctuate(point, max_fluctuation=.1, r=1):
    theta = np.random.rand() * np.pi
    phi = np.random.rand() * 2 * np.pi
    r *= max_fluctuation * np.random.rand()
    x = r * np.cos(phi) * np.sin(theta)
    y = r * np.sin(phi) * np.sin(theta)
    z = r * np.cos(theta)
    new_point = point + np.array([x, y, z])
    return new_point


def compact_structure(structure, max_bond_dist):
    """
    takes a structure and compacts it so that all the atoms are
    at least a certain distance from at least one other atom

    :param max_bond_dist: the distance between two atoms before which they can be considered bonded
    :param structure: structure to compact
    :return: compacted structure
    """

    # atoms_left_to_assign = [i for i in range(len(structure))]
    bonds = get_bonds(structure, structure, indices=True)

    fragments = {i: {i} for i in range(len(structure))}
    for bond in bonds:
        if 0.0 < bond[0] < max_bond_dist:
            if fragments[bond[1]] and fragments[bond[2]]:
                fragments[bond[1]].update(fragments[bond[2]])
                fragments[bond[2]] = None
            # fragments
    to_pop = []
    for k, v in fragments.items():
        if v is None:
            to_pop.append(k)
    for k in to_pop:
        fragments.pop(k)

    fragments = [[structure[j] for j in fragment] for i, fragment in fragments.items()]
    # print('fragments')
    # for fragment in fragments:
    #     print(type(fragment), fragment)
    fragments.sort(key=lambda fragment: len(fragment), reverse=True)
    fragments = [Atoms(fragment) for fragment in fragments]
    # print('fragments')
    # for fragment in fragments:
    #     print(fragment)
    # for fragment in fragments:
    #     display_cluster(np.array([structure[atom] for atom in fragment]))
    # display_cluster(structure)
    largest_fragment = fragments.pop(0)
    # cluster = [structure[atom] for atom in largest_fragment]
    # cluster = np.array(cluster)

    while fragments:
        next_fragment = fragments.pop(0)
        # print('next fragment', next_fragment)
        # next_fragment = [structure[atom] for atom in next_fragment]
        # next_fragment = np.array(next_fragment)

        largest_fragment = combine_two_fragments(next_fragment, largest_fragment, max_bond_dist)
        # print('largest fragment', largest_fragment)
    # input()
    # display_cluster(cluster)
    return largest_fragment


def combine_two_fragments(unlocked_structure, locked_structure, max_distance) -> Union[np.ndarray, Atoms]:
    """
    literally the same as move near fragments except with opposite control blocks below

    compares distances between atoms in s1 with atoms in s2 and finds the bond with minimum distance

    if this distance is longer than the cutoff distance, it moves
    s2 closer to s1 so that the minimum distance is satisfied

    :param max_distance: maximum allowed shortest distance between an atom in static and an atom in dynamic
    :param unlocked_structure: atomic structure - is 'moved' onto the static structure if too far away
    :param locked_structure: atomic structure - does not move
    :return:
    """
    # print('locked structure', type(locked_structure), locked_structure)
    # print('unlocked structure', type(unlocked_structure), unlocked_structure)

    if isinstance(unlocked_structure, Atoms):
        # unlocked_type = Atoms
        unlocked_coordinates = unlocked_structure.positions.copy()
    elif isinstance(unlocked_structure, Atom):
        # unlocked_type = Atom
        unlocked_coordinates = np.array([unlocked_structure.position])
    else:
        # unlocked_type = np.ndarray
        unlocked_coordinates = unlocked_structure.copy()

    if isinstance(locked_structure, Atoms):
        locked_type = Atoms
        locked_coordinates = locked_structure.positions.copy()
    elif isinstance(locked_structure, Atom):
        locked_type = Atom
        locked_coordinates = np.array([locked_structure.position])
    else:
        locked_type = np.ndarray
        locked_coordinates = locked_structure.copy()
    # print('unlocked coords', unlocked_coordinates)
    # print('locked coords', locked_coordinates)

    bonds = get_bonds(unlocked_coordinates, locked_coordinates)

    min_bond = bonds[0]
    # print(min_bond)

    # shift = np.zeros(3)

    if min_bond[0] > max_distance:
        shift = min_bond[1] - min_bond[2]
        shift *= (max_distance - min_bond[0]) / min_bond[0]
        unlocked_coordinates += shift

    if locked_type == Atoms:
        unlocked_structure = unlocked_structure.copy()
        unlocked_structure.positions = unlocked_coordinates

        # print('locked structure', type(locked_structure), locked_structure)
        # print('unlocked structure', type(unlocked_structure), unlocked_structure)
        return locked_structure + unlocked_structure
    else:
        # print('locked', locked_coordinates)
        # print('unlocked', unlocked_coordinates)
        return np.vstack([locked_coordinates, unlocked_coordinates])

    # else:
    #     return loc
    #      combined = np.concatenate([locked_structure, unlocked_structure], axis=0)
    # self.display_cluster(combined)
    # return combined
