from scipy.spatial import ConvexHull
from .ga_utilities import *
from .ga_geometry import *
from .ga_parameters import *
from typing import Union, Optional
import time


# POP_SIZE = 'pop_size'
# MAX_BOND_DIST = 'max_bond_dist'
# MIN_BOND_DIST = 'min_bond_dist'
# PREFERRED_BOND_DIST = 'preferred_bond_dist'
# REQUIRED_BONDS = 'required_bonds'
# SMALL_CLUSTER_CUTOFF = 'small_cluster_cutoff'
# GEN_BREADTH = 'gen_breadth'
# SMALL_CLUSTER_MAX_GENS = 'small_cluster_max_gens'
# LARGE_CLUSTER_MAX_GENS = 'large_cluster_max_gens'
# CROSS_MAX_GENS = 'cross_max_gens'
# MUTATION_MAX_GENS = 'mutation_max_gens'


class Bond:

    def __init__(self, a, b, max_dist, min_dist, preferred_dist):
        self.a = a
        self.b = b
        self.max_dist = max_dist
        self.min_dist = min_dist
        self.preferred_dist = preferred_dist


#     TODO make comparable/unique


class Individual:

    def __init__(self, atoms, name=None, mom=None, dad=None):
        self.atoms = atoms
        self.name = random_string() if name is None else name
        self.composition = composition_from_chemical_formula(atoms.get_chemical_formula())[0]
        self.support = None
        self.mount_vector = None
        self.energy = None
        self.is_relaxed = False
        self.fitness = None
        self.selection_probability = None
        self.mom = mom
        self.dad = dad

    def get_mounted_atoms(self, mount_vector=None):
        if mount_vector is not None:
            assert isinstance(mount_vector, np.ndarray) and mount_vector.shape == (3,)
        else:
            mount_vector = self.mount_vector
        assert isinstance(self.atoms, Atoms)
        atoms = self.atoms.copy()
        atoms.positions += mount_vector
        return atoms

    def get_supported_structure(self, support_structure=None, mount_vector=None):
        if support_structure is None:
            if self.support is None:
                return self.atoms.copy()
            else:
                support_structure = self.support
        assert isinstance(support_structure, Atoms)
        return support_structure + self.get_mounted_atoms(mount_vector)

    def __repr__(self):
        return 'Individual(%s %a energy: %a - %s)' % (
            self.name, self.composition, '' if self.energy is None else self.energy,
            'gas phase' if self.support is None else 'supported')

    def __str__(self):
        return 'Individual(%s %a energy: %a - %s)' % (
            self.name, self.composition, '' if self.energy is None else self.energy,
            'gas phase' if self.support is None else 'supported')

    def matches(self, other):
        return match_structures(other, self)


class GenePool:

    # NECESSARY_PARAMETERS = [POP_SIZE, MAX_BOND_DIST, MIN_BOND_DIST,
    #                         PREFERRED_BOND_DIST, REQUIRED_BONDS,
    #                         SMALL_CLUSTER_CUTOFF, GEN_BREADTH,
    #                         SMALL_CLUSTER_MAX_GENS, LARGE_CLUSTER_MAX_GENS,
    #                         CROSS_MAX_GENS, MUTATION_MAX_GENS]

    def __init__(self, system_parameters: SystemParameters,
                 generation_parameters: GenerationParameters,
                 logger, tracker, support=None):
        self.minima = []
        self.ikoth = 0  # iterations king of the hill - n iterations the GP's optima has 'reigned'
        self.composition = system_parameters.composition
        self.sys_params = system_parameters
        self.gen_params = generation_parameters
        # self.pop_size = pop_size
        # self.max_bond_dist = max_bond_dist
        # self.min_bond_dist = min_bond_dist
        # self.preferred_bond_dist = preferred_bond_dist
        # self.required_bonds = required_bonds
        # self.small_cluster_cutoff = small_cluster_cutoff
        # self.gen_breadth = 5
        # self.small_cluster_max_gens = 50000
        # self.large_cluster_max_gens = 10000
        # self.cross_max_gens = 5000
        # self.mutation_max_gens = 5000

        self.logger = logger
        self.tracker = tracker
        self.support = support

    def attempt_add_minima(self, cluster: Individual):
        if not self.minima:
            self.minima.append(cluster)
            return 'added'
        else:
            return [cluster.matches(minimum) for minimum in self.minima]

    def _generate_small_cluster(self, number_of_atoms: int, breadth: Union[int, float],
                                min_bond_dist=None, max_bond_dist=None, preferred_bond_dist=None):
        """
        generates a small cluster of atoms (n less than self.ga_params.small_cluster_cutoff)
        using the old generation algorithm
        :param number_of_atoms: n atoms in the cluster
        :param breadth: radial width of sample that a coordinate can be generated within
        :param min_bond_dist: minimum allowed bond distance between atoms
        :param max_bond_dist: maximum allowed bond distance between atoms
        :param preferred_bond_dist: preferred bond distance between atoms
        :return: returns coordinates of atoms in the generated cluster (that doesn't have bad bonds)
        """
        # start_time = time.time()
        # self.print('generating small cluster (%d)' % number_of_atoms, LOW)
        if number_of_atoms > self.gen_params.small_cluster_cutoff:
            raise ValueError('number of atoms in small cluster cannot exceed self.small_cluster_cutoff: %d' %
                             self.gen_params.small_cluster_cutoff)
        min_bond_dist = self.sys_params.min_bond_dist if min_bond_dist is None else min_bond_dist
        max_bond_dist = self.sys_params.max_bond_dist if max_bond_dist is None else max_bond_dist
        preferred_bond_dist = self.sys_params.preferred_bond_dist if preferred_bond_dist is None \
            else preferred_bond_dist

        symbol = self.composition[0]

        small_cluster = None

        count = 0
        while small_cluster is None:
            count += 1
            # if count % 500 == 0:
            # self.print('%d' % count, VERY_LOW)
            initial_coordinates = np.random.uniform(-breadth, breadth, size=(number_of_atoms, 3))
            initial_structure = Atoms([symbol for _ in range(number_of_atoms)], initial_coordinates)
            compacted_structure = compact_structure(initial_structure, preferred_bond_dist)
            assert isinstance(compacted_structure, Atoms)
            if test_for_bad_bonds(compacted_structure, max_bond_dist, min_bond_dist, self.sys_params.required_bonds):
                # TODO can be a huge bottleneck if n_required_bonds > 2, improve somehow
                #   maybe say if 95% of bonds are good, then the structure is good (with exceptions, etc...)
                continue
            if count >= self.gen_params.small_cluster_max_gens:
                raise GAException('%d small cluster max generation attempts reached' %
                                  self.gen_params.small_cluster_max_gens)
            assert isinstance(compacted_structure, Atoms)
            small_cluster = compacted_structure

        self.logger.log('small cluster generated after %d attempts' % count)

        individual = Individual(small_cluster)
        #
        # finish_time = time.time()
        #
        # # self.stats.new_small_generation(start_time, finish_time, count, individual.name)
        # # TODO tracker.track(SMALL_GENERATION, start_time, finish_time, (count, individual.name))
        return individual, count

    def generate_cluster(self, number_of_atoms: int = None, breadth: Union[int, float] = None,
                         max_bond_dist=None, min_bond_dist=None,
                         required_bonds=None, support=None) -> Individual:
        """

        :param number_of_atoms: number of atoms in cluster
        :param breadth: radial width of sample that a coordinate can be generated within
        :param max_bond_dist: maximum allowed bond distance between atoms
        :param min_bond_dist: minimum allowed bond distance between atoms
        :param required_bonds: number of bonds required for each atom
        :param support: structure of support cluster can be mounted onto
        :return: coordinates of atoms in the cluster
        """

        class ClusterGeneration(Tracker.Trackable):

            def __init__(self, subject, start, finish, iterations):
                super().__init__(self.__class__.__name__, subject, start, finish, iterations=iterations)

        # symbol = self.system_params.composition[0]
        number_of_atoms = self.composition[1] if number_of_atoms is None else number_of_atoms
        breadth = self.gen_params.gen_breadth if breadth is None else breadth
        max_bond_dist = self.sys_params.max_bond_dist if max_bond_dist is None else max_bond_dist
        min_bond_dist = self.sys_params.min_bond_dist if min_bond_dist is None else min_bond_dist
        required_bonds = self.sys_params.required_bonds if required_bonds is None else required_bonds
        support = self.support if support is None else support

        start_time = time.time()

        count = 0
        cluster = None

        if number_of_atoms > self.gen_params.small_cluster_cutoff:
            # print('starting with small cluster')

            while cluster is None:
                try:
                    cluster, attempts = self._generate_small_cluster(self.gen_params.small_cluster_cutoff, breadth)
                    count += attempts
                except GAException:
                    continue

            assert isinstance(cluster, Individual)

            atoms_remaining = number_of_atoms - self.gen_params.small_cluster_cutoff

            hull = ConvexHull(cluster.atoms.positions)
            facets: List[np.ndarray] = []
            for simplex in getattr(hull, 'simplices'):
                assert simplex.shape == (3,)

                facets.append(np.array([cluster.atoms[simplex[0]].position,
                                        cluster.atoms[simplex[1]].position,
                                        cluster.atoms[simplex[2]].position]))

            # self.print('generating larger cluster', LOW)
            # TODO logger.log('generating larger cluster', LOW)

            # TODO figure out some way to make growing the cluster crystal more efficient or learned
            #   combine facets that are close
            #   3+ simplex facets

            # TODO change, add, remove, etc biases associated with this generation algorithm
            #   prefer to place clusters closer to the center of the crystal
            #   do not repeat attempts to the same potential position when adding atoms

            while atoms_remaining > 0:
                # for i in range(outer_attempts):
                count += 1
                if count >= self.gen_params.large_cluster_max_gens:
                    raise GAException('%d large cluster max generation attempts reached' %
                                      self.gen_params.large_cluster_max_gens)
                # if count % 50 == 0:
                #     print('%d atoms remaining to be added to cluster (try number %d)' % (atoms_remaining, count))
                facet_index = random.choice([i for i in range(len(facets))])
                facet = facets[facet_index]
                assert isinstance(facet, np.ndarray) and facet.shape == (3, 3)

                a, b, c = facet
                assert a.shape == b.shape and b.shape == c.shape and c.shape == (3,)

                centroid = get_center(facet)
                assert centroid.shape == (3,)

                facet_normal = np.cross(b - a, c - a)
                if np.dot(facet_normal, centroid) < 0:  # make sure facet normal is facing correct direction
                    facet_normal *= -1.0

                centroid_dists = [distance(a, centroid),
                                  distance(b, centroid),
                                  distance(c, centroid)]

                closest_dist = None  # arbitrary - no atoms should spawn outside of a diameter of 2*breadth
                for i, dist in enumerate(centroid_dists):
                    if closest_dist is None or dist < closest_dist:
                        closest_dist = dist
                height = (min_bond_dist ** 2 + closest_dist ** 2) ** .5
                assert isinstance(height, float)

                fluctuation = 1 + np.random.rand() * self.gen_params.max_fluctuation
                starting_new_point = centroid + (facet_normal / np.linalg.norm(facet_normal) * height * fluctuation)
                assert starting_new_point.shape == (3,)

                for j in range(self.gen_params.facet_attempts):
                    new_point = fluctuate(starting_new_point, self.gen_params.max_fluctuation, height)
                    assert new_point.shape == (3,)

                    potential_coordinates = np.vstack([cluster.atoms.positions, new_point])
                    assert potential_coordinates.shape == (number_of_atoms - (atoms_remaining - 1), 3)

                    if not test_for_bad_bonds(potential_coordinates, max_bond_dist,
                                              min_bond_dist, required_bonds):
                        cluster.atoms += Atom(self.composition[0], new_point)
                        assert isinstance(cluster, Individual)

                        facets.pop(facet_index)
                        facets.append(np.array([a, b, new_point]))
                        facets.append(np.array([b, c, new_point]))
                        facets.append(np.array([a, c, new_point]))
                        assert isinstance(facets, list)

                        atoms_remaining -= 1

            self.logger.log('cluster generated after %d attempts' % count)
        else:
            while cluster is None:
                try:
                    cluster, attempts = self._generate_small_cluster(number_of_atoms, breadth)
                    count += attempts
                except GAException:
                    continue

        cluster.atoms.positions -= get_center(cluster.atoms.positions)
        # print(cluster.atoms.get_center_of_mass())
        # assert all(cluster.atoms.get_center_of_mass() == np.zeros(3))

        assert len(cluster.atoms) == number_of_atoms

        if support is not None:
            self.mount(cluster, support)

        finish_time = time.time()

        self.tracker.track(ClusterGeneration(cluster.name, start_time, finish_time, count))

        return cluster

    def mount(self, cluster: Individual, support, attempts=0, fluctuation=.05):
        assert isinstance(cluster.atoms, Atoms)
        assert isinstance(support, Atoms)
        atoms = cluster.atoms.copy()
        cluster_center = get_center(atoms)
        assert isinstance(cluster_center, np.ndarray) and cluster_center.shape == (3,)
        atoms.positions -= get_center(atoms)
        support = support.copy()
        support_center = get_center(support)
        assert isinstance(support_center, np.ndarray) and support_center.shape == (3,)
        cluster_z_rad = min([pos[2] for pos in atoms.positions])
        support_z_rad = max([pos[2] - support_center[2] for pos in support.positions])
        z_shift = 2 * (-cluster_z_rad + support_z_rad)

        mount_vector = None
        while mount_vector is None:
            x_shift = attempts * np.random.uniform(-fluctuation, fluctuation)
            y_shift = attempts * np.random.uniform(-fluctuation, fluctuation)
            starting_mount_vector = support_center + np.array([x_shift, y_shift, z_shift])
            atoms.positions += starting_mount_vector
            # cluster_atoms = []
            combined_structure = combine_two_fragments(atoms, support, self.sys_params.max_bond_dist)
            assert isinstance(combined_structure, Atoms)
            for atom in combined_structure:
                if atom.symbol in cluster.atoms.symbols.get_chemical_formula():
                    mount_vector = atom.position - cluster.atoms[0].position
                    break
            if test_for_bad_bonds(combined_structure, self.sys_params.max_bond_dist,
                                  self.sys_params.min_bond_dist, self.sys_params.required_bonds):
                mount_vector = None
                attempts += 1
        cluster.support = support
        cluster.mount_vector = mount_vector

    @staticmethod
    def demount(cluster: Individual, structure: Atoms, energy: float):
        assert isinstance(structure, Atoms) and (isinstance(energy, float) or isinstance(energy, int))

        gp_atoms = []
        for atom in structure:
            if atom.symbol == cluster.composition[0]:
                gp_atoms.append(atom)
        gp_atoms = Atoms(gp_atoms)
        assert isinstance(gp_atoms, Atoms)

        gp_center = get_center(gp_atoms)
        assert isinstance(gp_center, np.ndarray) and gp_center.shape == (3,)

        gp_coords = gp_atoms.positions - gp_center
        mount_vector = gp_atoms.positions[0] - gp_coords[0]
        gp_atoms.positions -= mount_vector
        cluster.atoms = gp_atoms
        cluster.mount_vector = mount_vector
        cluster.energy = energy
        cluster.is_relaxed = True

        return cluster

    # def get_fitnesses(self) -> Dict[str, float]:
    #     """
    #     Returns dict of names: fitnesses of all individuals
    #
    #     :return: dictionary with the fitnesses of all individuals
    #     """
    #     # TODO fix roulette to take arbitrary keys as options
    #     return {str(i): ind.fitness for i, ind in enumerate(self.minima)}
    #
    # def select(self) -> Individual:
    #     """
    #     function that selects an individual from the pool based on fitness using a weighted roulette
    #
    #     :return: selected individual
    #     """
    #     # TODO fix roulette to take arbitrary keys as options
    #     return self.minima[int(roulette(self.get_fitnesses()))]

    def cross(self, mom: Individual, dad: Individual) -> Optional[Individual]:

        class CrossOperation(Tracker.Trackable):

            def __init__(self, subject, start, finish, iterations, mother, father):
                super().__init__(self.__class__.__name__, subject, start, finish,
                                 iterations=iterations, mother=mother, father=father)

        self.logger.log('crossing')
        self.logger.log('mom %a' % mom)
        self.logger.log('dad %a' % dad)
        assert isinstance(mom, Individual) and isinstance(dad, Individual)
        # self.increment_age_stat(CROSSES_DONE)
        offspring = None

        start_time = time.time()

        def splice(a_atoms: Atoms, b_atoms: Atoms, cutting_plane):
            assert isinstance(a_atoms, Atoms) and isinstance(b_atoms, Atoms)

            a_halves = [[], []]
            for a in a_atoms:
                if np.dot(a.position, cutting_plane) < 0:
                    a_halves[0].append(a)
                else:
                    a_halves[1].append(a)

            b_halves = [[], []]
            for b in b_atoms:
                if np.dot(b.position, cutting_plane) < 0:
                    b_halves[0].append(b)
                else:
                    b_halves[1].append(b)

            return Atoms(a_halves[0] + b_halves[1]), Atoms(a_halves[1] + b_halves[0])

        count = 0
        while offspring is None:
            if count > self.gen_params.cross_max_gens:
                raise GAException('maximum cross attempts reached %d' % self.gen_params.cross_max_gens)
            child = None

            children = splice(mom.atoms, dad.atoms, 2 * np.random.rand(3) - 1)

            if len(children[0]) == len(mom.atoms):
                child = children[0]
            elif len(children[1]) == len(mom.atoms):
                child = children[1]

            count += 1

            if child is None:
                continue

            if test_for_bad_bonds(child, self.sys_params.max_bond_dist,
                                  self.sys_params.min_bond_dist, self.sys_params.required_bonds):
                continue

            offspring = Individual(child, mom=mom, dad=dad)
            if mom.support is not None:
                assert dad.support is not None
                offspring.mount_vector = (mom.mount_vector + dad.mount_vector) / 2
                offspring.support = mom.support

        self.logger.log('crossed successfully after %d attempts' % count)

        finish_time = time.time()

        self.tracker.track(CrossOperation(offspring.name, start_time, finish_time, count, mom.name, dad.name))

        return offspring

    def mutate(self, individual, mutation_proportion=.5, rotate=True) -> Individual:
        """
        takes an individual, shifts a proportion of its atoms in one direction, and then rotates the entire structure

        :param individual: structure to be mutated
        :param mutation_proportion: proportion of atoms in structure that will be shifted
        :param rotate: whether or not to rotate in addition to translate atoms
        :return: mutated cluster
        """
        assert isinstance(individual, Individual)
        assert individual.mount_vector is not None
        class MutationOperation(Tracker.Trackable):

            def __init__(self, subject, start, finish, iterations, host):
                super().__init__(self.__class__.__name__, subject, start, finish,
                                 iterations=iterations, host=host)

        start_time = time.time()

        # self.increment_age_stat(MUTATIONS_DONE)
        self.logger.log('mutating')
        # TODO FEATURES TO ADD:
        #   lineage system
        #   stretch and shrink bonds
        #   if single bond, then make sure structure rotates correctly
        #   perhaps less care if multiple bonds?

        mutated_individual = None

        count = 0
        while mutated_individual is None:
            if count > self.gen_params.mutation_max_gens:
                raise GAException('maximum mutation attempts reached %d' % self.gen_params.mutation_max_gens)
            mutated_atoms = individual.atoms.copy()

            indices_to_mutate = []
            indices_available = [i for i in range(len(mutated_atoms))]
            n_atoms_to_mutate = mutation_proportion * len(mutated_atoms)
            while len(indices_to_mutate) < n_atoms_to_mutate:
                choice = random.choice(indices_available)
                indices_to_mutate.append(choice)
                indices_available.remove(choice)

            dr = np.random.uniform(-.3, .5, 3)
            for index in indices_to_mutate:
                atom = mutated_atoms[index]
                assert isinstance(atom, Atom)
                atom.position += dr

            # mutated_coordinates = compact_structure(mutated_coordinates, self.system_params.preferred_bond_dist)
            mutated_atoms = compact_structure(mutated_atoms, self.sys_params.preferred_bond_dist)
            count += 1
            assert isinstance(mutated_atoms, Atoms)
            # print('mutated coordinates', type(mutated_coordinates), mutated_coordinates)
            # mutated_atoms = atoms_from_coordinates(self.system_params.composition[0], mutated_coordinates)

            if test_for_bad_bonds(mutated_atoms, self.sys_params.max_bond_dist,
                                  self.sys_params.min_bond_dist, self.sys_params.required_bonds):
                continue
            if rotate:
                mutated_atoms.rotate(np.random.uniform(0, 360), np.random.rand(3))
            mutated_atoms.positions -= mutated_atoms.get_center_of_mass()
            mutated_individual = Individual(mutated_atoms, mom=individual)
            # print(individual.mount_vector)
            assert individual.mount_vector.shape == (3,)
            mutated_individual.mount_vector = fluctuate(individual.mount_vector)
            mutated_individual.support = individual.support

        self.logger.log('mutated successfully after %d attempts' % count)

        finish_time = time.time()

        self.tracker.track(MutationOperation(mutated_individual.name, start_time, finish_time, count, individual.name))

        return mutated_individual

    def write_minima(self, keep_best_proportion, optima_dir):
        if not os.path.isdir(optima_dir):
            os.makedirs(optima_dir)
        n_keep = int(len(self.minima) * keep_best_proportion)
        n_keep = 1 if n_keep == 0 else n_keep

        from ase.io import write
        for individual in self.minima[:n_keep]:
            write(os.path.join(optima_dir, 'POSCAR.' + individual.name), individual.get_supported_structure())
