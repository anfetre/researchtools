from .ga_genetics import *


class ProductionSchema:

    def __init__(self, gene_pool: GenePool, select: callable,
                 algo_params: AlgorithmParameters, logger: Logger, tracker: Tracker):
        self.gene_pool = gene_pool
        self.select = select
        self.algo_params = algo_params
        self.logger = logger
        self.tracker = tracker

    def __call__(self, n_new_individuals):

        offsprings = []

        while len(offsprings) < n_new_individuals:
            offspring = None

            while offspring is None:
                odds = np.random.rand(3)

                if odds[0] < self.algo_params.cross_prob and odds[1] > self.algo_params.mutation_prob:  # just cross
                    # continue
                    self.logger.log('just crossing')
                    mom = self.select()
                    dad = self.select()
                    while mom == dad:
                        dad = self.select()
                    assert isinstance(mom, Individual) and isinstance(dad, Individual)

                    try:
                        offspring = self.gene_pool.cross(mom, dad)
                    except GAException:
                        continue

                elif odds[0] < self.algo_params.cross_prob and odds[1] > self.algo_params.mutation_prob:  # just mutate
                    self.logger.log('just mutating')
                    individual = self.select()
                    assert isinstance(individual, Individual)

                    try:
                        offspring = self.gene_pool.mutate(individual)
                    except GAException:
                        continue

                elif odds[0] > self.algo_params.cross_prob \
                        and odds[1] > self.algo_params.mutation_prob:  # both cross and mutate
                    mom = self.select()
                    dad = self.select()
                    while mom == dad:
                        dad = self.select()
                    assert isinstance(mom, Individual) and isinstance(dad, Individual)

                    if odds[0] > odds[1]:
                        self.logger.log('cross and then mutate')
                        try:
                            offspring = self.gene_pool.cross(mom, dad)
                            offspring = self.gene_pool.mutate(offspring)
                        except GAException:
                            continue
                    else:
                        if odds[2] > .667:
                            self.logger.log('mutate both then cross')
                            try:
                                mom = self.gene_pool.mutate(mom)
                                dad = self.gene_pool.mutate(dad)  # mutate both parents
                            except GAException:
                                continue
                        elif odds[2] > .333:
                            self.logger.log('mutating one then crossing')
                            try:
                                dad = self.gene_pool.mutate(dad)  # just mutate one
                            except GAException:
                                continue
                        else:
                            self.logger.log('mutating the other then crossing')
                            try:
                                mom = self.gene_pool.mutate(mom)  # just mutate the other
                            except GAException:
                                continue
                        try:
                            offspring = self.gene_pool.cross(mom, dad)
                        except GAException:
                            continue
            attempt_individual_insertion(offspring, offsprings, 'new individuals', self.logger)
        return offsprings
