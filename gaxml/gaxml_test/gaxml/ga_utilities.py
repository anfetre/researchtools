import string
import random
import os
import numpy as np
from typing import Tuple, List, Dict

VERY_LOW = 4
LOW = 3
MEDIUM = 2
HIGH = 1
CRITICAL = 0


RESEARCH_TOOLS_DIRECTORY = os.getcwd().split('researchtools')[0] + 'researchtools'


def join_with_research_tools_dir(sub_path):
    return os.path.join(RESEARCH_TOOLS_DIRECTORY, sub_path)


class GAException(Exception):

    def __init__(self, text):
        self.text = text

    def __repr__(self):
        return 'GAException: %s' % self.text

    def __str__(self):
        return 'GAException: %s' % self.text


def null(*args, **kwargs):
    return


LOG_HEADER = """

#################
##### GAxML #####
#################
"""


class Logger:

    def __init__(self, filename='', write_mode='a', template='{}'):
        self.write_mode = write_mode
        self._template = template
        if filename == '':
            self.log = print
            self.filename = 'std out'
        elif filename is None:
            self.log = null
            self.filename = 'n/a'
        else:
            self.filename = filename
        self.log(LOG_HEADER)

    @property
    def template(self):
        return self._template

    @template.setter
    def template(self, template):
        if '{}' in template:
            self._template = template

    def log(self, message: str):
        message = self.template.format(message)
        with open(self.filename, self.write_mode, newline='\n') as f:
            f.write(message + '\n')

    def __repr__(self):
        return 'Logger >> %s' % self.filename

    def __str__(self):
        return 'Logger >> %s' % self.filename


class Tracker:

    class Trackable:

        def __init__(self, label, subject, start, finish, **details):
            self.label = label
            self.subject = subject
            self.start = start
            self.finish = finish
            self.details = details

        def pretty_details(self):
            return '\n'.join(['{}: {}'.format(k, v) for k, v in self.details.items()])

        def __repr__(self):
            return "{}: '{}'\ns: {} - f: {}\n{}\n##########".format(self.label, self.subject, self.start,
                                                                    self.finish, self.pretty_details())

        def __str__(self):
            return "{}: '{}'\ns: {} - f: {}\n{}\n##########".format(self.label, self.subject, self.start,
                                                                    self.finish, self.pretty_details())

    def __init__(self, label, stats_dirname=None):
        self.label = label
        self.stats_dirname = stats_dirname
        if self.stats_dirname is not None:
            if not os.path.isdir(self.stats_dirname):
                os.makedirs(self.stats_dirname)
        self.stats = [[]]

    def track(self, trackable: Trackable):
        self.stats[-1].append(trackable)

    def iterate(self, save=True):
        if save:
            with open(os.path.join(self.stats_dirname, '{}_{}.txt'.format(self.label, len(self.stats))), 'w') as f:
                for trackable in self.stats[-1]:
                    f.write(str(trackable))
        self.stats.append([])


def composition_from_chemical_formula(formula: str) -> List[Tuple[str, int]]:
    composition = []
    element = []
    number = []
    formula = list(formula)
    while formula:
        char = str(formula.pop(0))
        if char.isalpha():
            if len(number) > 0:
                composition.append((''.join(element), int(''.join(number))))
                element = []
                number = []
            element.append(char)
        else:
            number.append(char)

    if number:
        composition.append((''.join(element), int(''.join(number))))

    return composition


def random_string(length=8, chars=string.ascii_letters + string.digits):
    return ''.join(random.SystemRandom().choice(chars) for _ in range(length))


ALPHABETIC = "abcdefghijklmnopqrstuvwxyz"


def roulette(option_weights: Dict[str, float], fill_option: str = None) -> str:
    # print(option_weights)
    total_weight = sum(option_weights.values())
    if total_weight < 1.0:
        if fill_option is not None:
            option_weights[fill_option] = 1.0 - total_weight
            total_weight += option_weights[fill_option]
        standardized_option_weights = [v / total_weight for v in option_weights.values()]
    else:
        # print(total_weight)
        standardized_option_weights = [v / total_weight for v in option_weights.values()]
    # print(standardized_option_weights)
    decision = np.random.rand()
    # print(decision)
    for i, k in enumerate(option_weights.keys()):
        if decision <= sum(standardized_option_weights[0:i + 1]):
            # print(i, k)
            return k
    else:
        return "fail"


def random_alphabetic_weights(n_options: int, upper: int, lower: int = None) -> Dict[str, float]:
    if lower is None:
        lower = 0
    options = {ALPHABETIC[i]: np.random.rand() + random.randint(lower, upper) for i in range(0, n_options)}
    return options


def random_numeric_weights(n_options: int, upper: int, lower: int = None) -> Dict[str, float]:
    if lower is None:
        lower = 0
    options = {str(i): np.random.rand() + random.randint(lower, upper) for i in range(0, n_options)}
    return options


class Printer:

    def __init__(self, verbosity: int = 0):
        self.verbosity = verbosity

    def print(self, text: str, priority: int):
        if self.verbosity >= priority:
            print(text)
